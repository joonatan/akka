const { CheckerPlugin } = require('awesome-typescript-loader')

module.exports = {
  entry: 'src/index.jsx',
  output: {
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  resolve: {
    extensions: [ '.js', '.jsx', '.tsx', '.ts' ]
  },
  module: {
    rules: [
      { test: /\.(js|jsx)$/, use: 'babel-loader', exclude: /node_modules/ },
      { test: /\.ts(x)$/, loader: 'awesome-typescript-loader', exclude: /node_modules/ }
    ]
  },
  plugins: [
      new CheckerPlugin()
  ]
}
