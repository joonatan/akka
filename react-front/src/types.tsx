/* Joonatan Kuosa
 * 2020-01-02
 */

import { eqDate } from './dateUtils'
import { mod } from './utils'

// TODO fix links
export declare interface IReview {
}

// Job sturcture for both past and future work
// TODO needs date, location, customer, done flag, review
export declare interface IJob {
}

export enum Weekday {
  Monday = 0,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
  Sunday,
}

export class TimeInterval {
  morning: boolean
  afternoon: boolean
  evening: boolean

  constructor () {
    this.morning = true
    this.afternoon = true
    this.evening = true
  }
}

export declare interface IWorkTime {
  day: Weekday,
  startTime: number,
  stopTime: number,
}

export class WorkTime implements IWorkTime {
  day: Weekday
  startTime: number
  stopTime: number

  constructor (data : IWorkTime) {
    this.day = data.day
    this.startTime = data.startTime
    this.stopTime = data.stopTime
  }
}

export declare interface ICalendar {
  available: IWorkTime[],
  unavailable: Date[],
  jobs: IJob[],
}

export declare interface IWorker {
  id: number,
  calendar: ICalendar,
  name: string,   // TODO this can be aggregated from User
  user: string,   // TODO needs to be a link or BSON.id
  picture: string,
  info: string,
  desc: string,
  // TODO these can be aggregated from Jobs
  rating: number,
  reviews: string[],
}

export class Calendar implements ICalendar {
  available: IWorkTime[] = []
  unavailable: Date[] = []
  jobs: IJob[] = []

  // constructor for javascript objects
  constructor (data? : ICalendar) {
    this.available = data ? data.available.map((x : any) => new WorkTime(x)) : []
    this.unavailable = data ? data.unavailable.map((x : any) => new Date(x.date)) : []
    this.jobs = data ? data.jobs: []
  }
}

function inRange (val : IWorkTime, start : number, stop : number) {
  return val.startTime <= start && stop <= val.stopTime
}

export function available(worker : IWorker, date : Date, time? : TimeInterval) {
  const diff = mod(date.getDay() - 1, 6)
  const t = time || new TimeInterval()
  const workTime = worker.calendar.available.filter(x => (
    x.day === diff
    && (
      (t.morning && inRange(x, 8, 12))
      || (t.afternoon && inRange(x, 12, 16))
      || (t.evening && inRange(x, 16, 20))
    )
  ))
  const unavail = worker.calendar.unavailable.filter(x => eqDate(x, date)).length !== 0
  return !unavail && workTime.length !== 0
}

export class Worker implements IWorker {
  id: number = -1
  calendar: Calendar = new Calendar()
  name: string = ''  // TODO this can be aggregated from User
  user: string = ''  // TODO needs to be a link or BSON.id
  picture: string = ''
  info: string = ''
  desc: string = ''
  // TODO these can be aggregated from Jobs
  rating: number = -1
  reviews: string[] = []

  // constructor for javascript objects
  constructor (data : IWorker) {
    this.id = data.id
    this.calendar = new Calendar(data.calendar)
    this.name = data.name
    this.user = data.user
    this.picture = data.picture
    this.info = data.info
    this.desc = data.desc
    this.rating = Number(data.rating)
  }
}
