/* Joonatan Kuosa
 * 2020-01-27
 *
 * Akka - cleaning service app
 */
import { combineReducers } from 'redux'

import {
  FETCH_WORKERS_BEGIN,
  FETCH_WORKERS_FAILURE,
  FETCH_WORKERS_SUCCESS,
  IFetchWorkerState,
  FETCH_POSTCODES_BEGIN,
  FETCH_POSTCODES_FAILURE,
  FETCH_POSTCODES_SUCCESS,
  IFetchPostcodeState,
} from './actions'

const workerInitialState : IFetchWorkerState = {
  data: [],
  loading: false,
  error: null,
}

const postcodesInitialState : IFetchPostcodeState = {
  data: [],
  loading: false,
  error: null,
}

function workerReducer(state = workerInitialState, action : any) {
  switch(action.type) {
    case FETCH_WORKERS_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      console.log('worker fetch started')
      return {
        ...state,
        loading: true,
        error: null
      }

    case FETCH_WORKERS_SUCCESS:
      console.log('worker fetch finnished')
      return {
        ...state,
        loading: false,
        data: action.data.workers
      }

    case FETCH_WORKERS_FAILURE:
      // The request failed. It's done. So set loading to "false".
      return {
        ...state,
        loading: false,
        error: action.data.error,
        data: []
      }

    default:
      return state
  }
}

function postcodeReducer(state = postcodesInitialState, action : any) {
  switch(action.type) {
    case FETCH_POSTCODES_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      console.log('postcode fetch started')
      return {
        ...state,
        loading: true,
        error: null
      }

    case FETCH_POSTCODES_SUCCESS:
      console.log('postcode fetch finnished')
      return {
        ...state,
        loading: false,
        data: action.data.postcodes
      }

    case FETCH_POSTCODES_FAILURE:
      // The request failed. It's done. So set loading to "false".
      return {
        ...state,
        loading: false,
        error: action.data.error,
        data: []
      }

    default:
      return state
  }
}

export default combineReducers({
  workers: workerReducer,
  postcodes: postcodeReducer,
})
