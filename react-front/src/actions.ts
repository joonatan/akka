/* Joonatan Kuosa
 * 2020-01-27
 *
 * Akka - cleaning service app
 */
import axios from 'axios'

import { Worker } from './types'

export const FETCH_WORKERS_BEGIN = 'FETCH_WORKERS_BEGIN'
export const FETCH_WORKERS_FAILURE = 'FETCH_WORKERS_FAILURE'
export const FETCH_WORKERS_SUCCESS = 'FETCH_WORKERS_SUCCESS'

export const FETCH_POSTCODES_BEGIN = 'FETCH_POSTCODES_BEGIN'
export const FETCH_POSTCODES_FAILURE = 'FETCH_POSTCODES_FAILURE'
export const FETCH_POSTCODES_SUCCESS = 'FETCH_POSTCODES_SUCCESS'

export declare interface IFetchWorkerState {
  data: Worker[],
  loading: boolean,
  error: string | null,
}

export declare interface IFetchPostcodeState {
  data: string[],
  loading: boolean,
  error: string | null,
}

export const fetchWorkers = () => async (dispatch : any) => {
  const url = "/workers"

  dispatch({
    type: FETCH_WORKERS_BEGIN
  })
  // Use any here since Axios returns javascript object
  // it parses the strings to objects but doesn't call custom JSON parsers
  // so everything it returns is a string or a collection of {key: val}
  axios.get<any[]>(url).then((response : any) => {
    const parsed : Worker[] = response.data.map((x : any) => new Worker(x))

    dispatch({
      type: FETCH_WORKERS_SUCCESS,
      data: { workers: parsed},
    })
  }).catch((err) => {
    console.error('failed to get workers: ', err)
    dispatch({
      type: FETCH_WORKERS_FAILURE,
      data: { error: err.message },
    })
  })
}

export const fetchPostcodes= () => async (dispatch : any) => {
  const url = "/postcodes"

  dispatch({
    type: FETCH_POSTCODES_BEGIN
  })
  axios.get<string[]>(url).then((response : any) => {

    dispatch({
      type: FETCH_POSTCODES_SUCCESS,
      data: { postcodes: response.data},
    })
  }).catch((err) => {
    console.error('failed to get workers: ', err)
    dispatch({
      type: FETCH_POSTCODES_FAILURE,
      data: { error: err.message },
    })
  })
}
