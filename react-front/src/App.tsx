/* Joonatan Kuosa
 * 2020-01-02
 *
 * Akka - cleaning service app
 */
import React, { useState, useEffect } from 'react'
import { connect } from "react-redux";

import { Container, Grid, AppBar, Toolbar, Button, Menu, MenuItem, makeStyles } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

import WorkerList from './components/WorkerList'
import WorkerModal from './components/WorkerModal'
import Calendar from './components/Calendar'
import PostCodeForm from './components/PostCodeForm'
import OrderPage from './components/OrderPage'

import { Worker, IWorker, TimeInterval, available } from './types'

import { fetchWorkers, fetchPostcodes, IFetchWorkerState, IFetchPostcodeState } from './actions'

const STORAGE_PAGE = 'akka_page'

const useStyles = makeStyles((theme) => ({
  offset: theme.mixins.toolbar,
  login: {
  },
}))

enum Page {
  Lander = 0,
  Postcode,
  Selection,
  Order,
  Confirmation  // Confirmation is Thank you page
}

// Check that the post code is in our database (the ones we service)
const validPostCode = (code : string) => {
  return (/\d\d\d\d\d/g.test(code))
}

declare interface AppProps {
  error : null | string,
  loading : boolean,
  workers : Worker[],
  postcodes : string[],
  fetchWorkers : any,
  fetchPostcodes: any,
}

const App: React.FC<AppProps> = (props) => {
  const classes = useStyles()

  const [modalOpen, setModalOpen] = useState(false)
  const [previewWorker, setPreviewWorker] = useState<IWorker | undefined>(undefined)
  const [selectedWorker, setSelectedWorker] = useState<IWorker | undefined>(undefined)
  const [time, setTime] = useState(new TimeInterval())
  const [date, setDate] = useState(new Date())
  const [page, setPage] = useState<Page>(Page.Postcode)
  const [postcode, setPostcode] = useState<string>('')

  const { workers, postcodes } = props

  // Alert messages for user
  const [flash, setFlash_] = useState<string>('')
  const [error, setError_] = useState<string>('')

  const setFlash= (msg : string) => {
    setFlash_(msg)
    setTimeout(() => setFlash_(''), 2000)
  }
  const setError = (err : string) => {
    setError_(err)
    setTimeout(() => setError_(''), 2000)
  }

  useEffect(() => {
    if (props.error) {
      setError(props.error)
    }
  }, [props])

  useEffect(() => {
    props.fetchWorkers()
    props.fetchPostcodes()
  }, [])

  useEffect(() => {
    const p = window.localStorage.getItem(STORAGE_PAGE)
    if (p) {
      setPage(Number(p))
    }
  }, [])

  useEffect(() => {
    window.localStorage.setItem(STORAGE_PAGE, String(page))
  }, [page])

  const canService = (code : string) => {
    return postcodes.filter(x => x === code).length !== 0
  }

  const handleWorkerPreview = (worker: IWorker) => {
    console.log('preview: ', worker.name)
    setPreviewWorker(worker)
    setModalOpen(true)
  }

  const handleWorkerSelected = (worker: IWorker | undefined) => {
    const n = worker ? worker.name : 'undefined'
    console.log('selected: ', n)
    setSelectedWorker(worker)
  }

  const handleCloseModal = () => {
    setModalOpen(false)
  }

  // TODO implement filtering based on this
  const handleDateChange = (date : Date) : void => {
    console.log('date changed: ', date)
    setDate(date)
  }

  const handleTimeChange = (time : TimeInterval) : void => {
    console.log('time changed: ', time)
    setTime(time)
  }

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const handleBack= () => {
    if (page > 0) {
      setPage(page - 1)
    }
  }

  const handleProceed = () => {
    switch (page) {
      case Page.Selection:
        if (selectedWorker && date && time) {
          setPage(Page.Order)
        } else {
          setError('Need to select a worker, date and a time.')
        }
        break
      case Page.Postcode:
        if (!validPostCode(postcode)) {
          setError('Invalid post code')
        } else if (!canService(postcode)) {
          setError('Can\'t service that location.')
        } else {
          setPage(page + 1)
        }
        break
      case Page.Lander:
      case Page.Order:
        setPage(page + 1)
        break
      case Page.Confirmation:
      default :
        setError('Incorrect page change')
        break
    }
  }

  const handlePostCode = (code : string) => {
    setPostcode(code)
  }

  // TODO add a stepper for the order process (Selection, Order, Confirmation)
  // TODO Add a sidebar widget with already selected information.
  // TODO Add a back button and a top bar that show the order procedure
  // TODO Add Routes so we can use browser back to go back on the order procedure
  return (
    <Container maxWidth="xl">
      <AppBar position="fixed">
        <Toolbar>
          <Grid container direction="row" justify="space-between">
            <h3>AKKA</h3>
            <Button>Login</Button>
          </Grid>
          <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
            Debug Menu
          </Button>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
          <MenuItem onClick={() => setPage(Page.Lander)}>Lander</MenuItem>
          <MenuItem onClick={() => setPage(Page.Postcode)}>Postcode</MenuItem>
          <MenuItem onClick={() => setPage(Page.Selection)}>Selection</MenuItem>
          <MenuItem onClick={() => setPage(Page.Order)}>Order</MenuItem>
          <MenuItem onClick={() => setPage(Page.Confirmation)}>Confirmation</MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>

      <div className={classes.offset} />
      <WorkerModal open={modalOpen} closeModal={handleCloseModal} worker={previewWorker} />

      <Grid
        container
        spacing={0}
        alignItems="center"
        justify="center"
        style={{ minHeight: '80vh', height: '80vh' }}
      >
        <Grid item style={{ textAlign: "center" }} xs={12}>
          {error !== '' && <Alert severity="error">{error}</Alert>}
          {flash !== '' && <Alert severity="success">{flash}</Alert>}
        </Grid>
        {page === Page.Selection && (
        <Grid container direction="row" spacing={3}>
          <Grid item xs={12} sm={6}>
            <h2>Select a date for cleaning</h2>
            <Calendar dateChanged={handleDateChange} timeChanged={handleTimeChange} />
          </Grid>
          <Grid item xs={12} sm={6}>
            <WorkerList workers={workers.filter(x => available(x, date, time))} previewWorkerCb={handleWorkerPreview} workerSelectedCb={handleWorkerSelected} />
          </Grid>
        </Grid>
        )}
        {page !== Page.Selection && (
        <Grid item xs={12}>
          {page === Page.Order && <OrderPage postcode={postcode} backCb={handleBack} nextCb={handleProceed} />}
          {page === Page.Postcode && <PostCodeForm postcode={postcode} nextCb={handleProceed} setPostCodeCb={handlePostCode} />}
          {page === Page.Confirmation && <div style={{ minHeight: '80vh' }}>Should render Order confirmation form</div>}
          {page === Page.Lander && <p style={{ minHeight: '80vh' }}>Should render lander</p>}
        </Grid>
        )}
      </Grid>
      {page !== Page.Order &&
        <Grid item style={{ textAlign: "center" }} xs={12}>
          <Button variant="contained" onClick={handleBack}>Back</Button>
          <Button variant="contained" onClick={handleProceed} color="primary">Proceed</Button>
        </Grid>
      }
    </Container>
  )
}

const mapStateToProps = (state : { postcodes : IFetchPostcodeState, workers : IFetchWorkerState } ) => ({
  postcodes: state.postcodes.data,
  workers: state.workers.data,
  loading: state.workers.loading,
  error: state.workers.error
})

const funcmap = {
  fetchWorkers,
  fetchPostcodes,
}

export default connect(mapStateToProps, funcmap)(App)
