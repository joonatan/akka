/*  Joonatan Kuosa
 *  2020-01-02
 *
 * Akka - cleaning service app
 */
import React from 'react'

import { Table, TableBody, TableHead, TableCell, TableRow, makeStyles, Theme } from '@material-ui/core'

import { ICalendar } from '../types'

import { days, monday, sunday, getDate, eqDate } from '../dateUtils'

const useStyles = makeStyles((theme: Theme) => ({
  schedule: {
    maxWidth: 640,
  },
}))

const WeeklySchedule = (props: { calendar : ICalendar }) => {
  const classes = useStyles()

  const { calendar } = props

  const unavailable : Date[] = calendar.unavailable
    .filter((d : Date) => {
      return monday <= d && d <= sunday
    })

  const cal = calendar.available.map(x => {
    const d = getDate(monday, x.day)
    const avail = unavailable.filter(x => eqDate(x, d)).length === 0
    return {
      day: days[x.day],
      n: x.day,
      date: d,
      morning: avail && x.startTime <= 8 && x.stopTime >= 12,
      afternoon: avail && x.startTime <= 12 && x.stopTime >= 16,
      evening: avail && x.startTime <= 16 && x.stopTime >= 20,
    }
  })

  const padding = cal.length > 0 ? cal[0].n : 7
  for (let i = 0; i < padding; ++i) {
    cal.unshift({ day: days[i], n: i, date: getDate(monday, i), morning: false, afternoon: false, evening: false })
  }

  for (let i = cal.length; i < days.length; ++i) {
    cal.push({ day: days[i], n: i, date: getDate(monday, i), morning: false, afternoon: false, evening: false })
  }

  const row1 = cal.map(x => ({ key: x.n, val: x.morning }))
  const row2 = cal.map(x => ({ key: x.n, val: x.afternoon }))
  const row3 = cal.map(x => ({ key: x.n, val: x.evening }))
  const table = [{name: 'Morning', row: row1}, {name: 'Afternoon', row: row2}, {name: 'Evening', row: row3}]


  const notAvailStyle = {
    backgroundColor: 'gray',
  }

  const availStyle = {
    backgroundColor: 'green',
  }

  // TODO remove past dates from the calendar
  // TODO add dates to the weekly calendar
  // TODO how to handle this in mobile/responsive?
  //    Replace all the named headers with short-hands (Monday => Mon)
  //    Remove the time or replace with a clock (8-12)
  // TODO add button to select a time from the Week (on the green fields)
  return (
    <Table className={classes.schedule}>
      <TableHead>
        <TableRow>
          {['', ...days].map(x => (
          <TableCell key={x}>
            {x}
          </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {table.map(row => (
          <TableRow key={row.name}>
            <TableCell key={row.name}>{row.name}
            </TableCell>
            {row.row.map((x : { key : number, val : boolean }) => (
              <TableCell key={x.key} style={x.val ? availStyle : notAvailStyle}>{x.val ? 'yes' : 'nope'}</TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}

export default WeeklySchedule
