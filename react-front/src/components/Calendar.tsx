/* Joonatan Kuosa
 * 2020-01-07
 *
 * Akka - cleaning service app
 */
import React, { useState, useEffect } from 'react'

import { makeStyles, Theme, Button, ButtonGroup } from '@material-ui/core'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

//import Calendar from 'react-calendar/dist/entry.nostyle'
import Calendar from 'react-calendar'

import TimeSelector from './TimeSelector'

import { TimeInterval } from '../types'

import { today, max, isSameMonth } from '../dateUtils'

const useStyles = makeStyles((theme: Theme) => ({
  calendar: {
    width: 'auto',
    height: 380,
    background: 'white',
    border: '1px solid #a0a096',
    fontFamily: 'Arial, Helvetica, sans-serif',
    fontSize: 20,
    lineHeight: '2em',
  },
}))

const Cal = (props: {
  dateChanged : (date: Date) => void,
  timeChanged : (time : TimeInterval) => void,
}) => {
  const [ date, setDate ] = useState(new Date())

  const classes = useStyles()

  const {dateChanged, timeChanged} = props

  const max_date = new Date(today.getFullYear(), today.getMonth() +1, today.getDate())

  useEffect(() => {
    dateChanged(date)
  }, [date, dateChanged])

  const handleThisMonth = () => {
    setDate(new Date())
  }

  const handlePrevMonth = () => {
    setDate(max(today, new Date(date.getFullYear(), date.getMonth() -1, 1)))
  }

  const handleNextMonth = () => {
    setDate(new Date(date.getFullYear(), date.getMonth() +1, 1))
  }

  const view_month_name = date.toLocaleString('default', { month: 'long' });

  return (
    <div>
      <ButtonGroup size='large' aria-label="large outlined primary button group">
       <Button onClick={handleThisMonth} >{view_month_name}</Button>
      </ButtonGroup>
      <Calendar className={classes.calendar}
        onChange={(date) => setDate(date as Date)}
        value={date}
        maxDate={max_date}
        minDate={today}
        showNavigation={false}
      />
      <ButtonGroup fullWidth={true}>
        <Button onClick={handlePrevMonth} disabled={isSameMonth(date, today)} aria-label="secondary" fullWidth={true} startIcon={<ArrowBackIcon />}>Prev</Button>
        <Button onClick={handleNextMonth} disabled={isSameMonth(date, max_date)} aria-label="secondary" fullWidth={true} endIcon={<ArrowForwardIcon />}>Next</Button>
      </ButtonGroup>
      <TimeSelector cb={timeChanged} />
    </div>
  )
}

export default Cal
