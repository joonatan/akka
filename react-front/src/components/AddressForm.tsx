/* Joonatan Kuosa
 * 2020-01-08
 *
 * Akka - cleaning service app
 */
import React, { useEffect, useRef } from 'react'

import { Theme, createStyles, makeStyles } from '@material-ui/core/styles'
import { TextField } from '@material-ui/core'

import PostCodeMask from './PostCodeMask'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '90vw',
        maxWidth: '600px',
      },
      display: 'flex',
      flexDirection: 'column',
    },
  }),
)

export declare interface IAddress {
  name : string,
  email : string,
  address : string,
  apt_number : number,
  postcode : string,
  other : string,
}

export const isAddressValid = (addr : IAddress | null) => {
  return addr && addr.name.length > 0 && addr.email.length > 0 && addr.address.length > 0
}

const AddressForm = (props: { postcode : string, cb : (addr : IAddress) => void }) => {
  const [name, setName] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [address, setAddress] = React.useState('')
  const [aptNumber, setAptNumber] = React.useState(0)
  const [postcode, setPostcode] = React.useState(props.postcode)
  const [other, setOther] = React.useState('')
  const postCodeRef = useRef<HTMLInputElement>(null)

  const classes = useStyles()

  useEffect(() => {
    props.cb({
      name: name,
      email: email,
      address: address,
      apt_number: aptNumber,
      postcode: postcode,
      other: other,
    })
  }, [name, email, address, aptNumber, postcode, other])

  // TODO apartment number should be a number
  return (
    <form className={classes.root} noValidate autoComplete="off">
      <TextField required label="Name" placeholder="Full name" onChange={(e) => setName(e.target.value)} />
      <TextField required type="email" label="Email" placeholder="Email" onChange={(e) => setEmail(e.target.value)} />
      <TextField required label="Address" placeholder="Address" onChange={(e) => setAddress(e.target.value)} />
      <TextField label="Apartment number" placeholder="Apartment number" onChange={(e) => setAptNumber(Number(e.target.value))} />
      <TextField
        label="Postcode"
        value={postcode}
        onChange={(e) => setPostcode(e.target.value)}
        id="postcode-input"
        ref={postCodeRef}
        InputProps= {{ inputComponent: PostCodeMask as any }}
      />

      <TextField label="Other information (door codes etc.)" onChange={(e) => setOther(e.target.value)} />
    </form>
  )
}

export default AddressForm
