/* Joonatan Kuosa
 * 2020-01-07
 *
 * Akka - cleaning service app
 */
import React from 'react'

import { Card, CardContent, CardActions, CardActionArea, CardMedia, Button, Typography, makeStyles, Theme } from '@material-ui/core'

import Rating from '@material-ui/lab/Rating'

import { IWorker } from '../types'

const useStyles = makeStyles((theme: Theme) => ({
  card: {
    display: 'flex',
    maxWidth: 645,
    justifyContent: 'flex-start',
  },
  title: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  content: {
    width: '100%',
  },
  cover: {
    width: 151,
  },
  media: {
    maxHeight: 140,
    maxWidth: 140,
    height: 140,
    width: 140,
  },
  rating: {
    marginLeft: 'auto',
  },
}))


// Pass the selected worker as an attribute so the parent controls how the selections are done
const Contractor = (props : {
  worker : IWorker,
  previewWorker : (worker: IWorker) => void,
  workerSelectedCb : (worker: IWorker | undefined) => void,
  selected : boolean,
}) => {
  const classes = useStyles()

  const { worker, previewWorker, workerSelectedCb, selected } = props

  // TODO add number of reviews to the rating like: 4.1 (10)
  return (
    <Card raised={selected}>
      <CardActionArea className={classes.card} onClick={() => previewWorker(worker)}>
        <CardMedia
            className={classes.media}
            image={worker.picture}
            title={worker.name}
        />
        <CardContent className={classes.content}>
          <div className={classes.title}>
            <Typography gutterBottom variant="h5" component="h2">
              {worker.name}
            </Typography>
            <Rating className={classes.rating} name="read-only" value={worker.rating} precision={0.5} readOnly />
          </div>
          <Typography variant="body2" component="p">
            {worker.desc}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        {selected
        ? <Button size="small" color="primary" onClick={() => workerSelectedCb(undefined)}>Deselect</Button>
        : <Button size="small" color="primary" onClick={() => workerSelectedCb(worker)}>Select</Button>
        }
      </CardActions>
    </Card>
  )
}

export default Contractor
