/* Joonatan Kuosa
 * 2020-01-11
 *
 * Akka - cleaning service app
 */
import React from 'react'

import { Grid, TextField } from '@material-ui/core'

import PostCodeMask from './PostCodeMask'


interface PostCodeFormProps {
  postcode : string,
  nextCb: () => void,
  setPostCodeCb: (postcode : string) => void,
}

// TODO add text checking: number inputs
// TODO autofocus isn't good enough when we fail to proceed
//  it doesn't refocus on failed proceed.
//  A solution to this is to disable the button
const PostCodeForm = (props : PostCodeFormProps) => {
  const inputEl = React.useRef<HTMLInputElement>(null)

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setPostCodeCb(event.target.value)
  }

  const handleKeyPress = (evt : any) => {
    if (evt.key === 'Enter') {
      props.nextCb()
    }
  }

  return (
    <div>
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: '85vh' }}
    >
      <Grid item xs={12}>
          <TextField
            label="Postcode"
            value={props.postcode}
            onChange={handleChange}
            onKeyPress={handleKeyPress}
            id="postcode-input"
            ref={inputEl}
            autoFocus
            InputProps= {{ inputComponent: PostCodeMask as any }}
          />
      </Grid>
    </Grid>
    </div>
  )
}

export default PostCodeForm
