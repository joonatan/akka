/* Joonatan Kuosa
 * 2020-01-08
 *
 * Akka - cleaning service app
 */
import React, { useEffect } from 'react'

import { Theme, createStyles, makeStyles } from '@material-ui/core/styles'
import { IconButton, Checkbox, TextField, FormControlLabel, FormLabel } from '@material-ui/core'
import { Add, Remove } from '@material-ui/icons'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '90vw',
        maxWidth: '600px',
      },
      display: 'flex',
      flexDirection: 'column',
    },
    roomSelector: {
      maxWidth: '90vw',
    },
    rooms: {
      maxWidth: 20,
      width: 20,
    },
    checkbox: {
      paddingLeft: 10,
      paddingTop: 10,
    },
  }),
)

// Data:
export declare interface IService {
  rooms : number,
  pets : string,
  vacuum : boolean,  // do they have a vacuum cleaner
  supplies : boolean, // do they have cleaning supplies
  allergies : string,
  other : string,
}

const ServiceForm = (props: { cb : (ser : IService) => void }) => {
  const [pets, setPets] = React.useState(false)
  const [vacuum, setVacuum] = React.useState(true)
  const [supplies, setSupplies] = React.useState(true)

  const [petInfo, setPetInfo] = React.useState('')
  const [rooms, setRooms] = React.useState(1)
  const [allergies, setAllergies] = React.useState('')
  const [other, setOther] = React.useState('')

  const classes = useStyles()

  useEffect(() => {
    props.cb({
      rooms: rooms,
      pets: String(pets) + ' : ' + petInfo,
      vacuum: vacuum,
      supplies: supplies,
      allergies: '',
      other: '',
    })
  }, [pets, vacuum, supplies, petInfo, rooms, allergies, other])

  const Toggle = (props : { checked : any, label : string, onChange : (e : any) => void} ) => (
    <FormControlLabel
      className={classes.checkbox}
      control={
        <Checkbox
          checked={props.checked}
          onChange={props.onChange}
          color="primary"
        />
      }
      label={props.label}
    />
  )

  // TODO can we add contained to IconButtons somehow?
  // TODO add error information if trying to reduce rooms below 1
  // TODO pets have too long of a text field, need to overwrite it? or have a max field length different
  //  we want the div to have the maxWidth
  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div className={classes.roomSelector}>
        <IconButton color="primary" aria-label="remove" onClick={() => rooms > 1 ? setRooms(rooms - 1) : {} }>
          <Remove />
        </IconButton>
        <TextField
          value={rooms}
          style={{ textAlign: 'center', width: 20, maxWidth: 20 }}
          disabled
        />
        <IconButton color="primary" aria-label="add" onClick={() => setRooms(rooms + 1)}>
          <Add />
        </IconButton>
        <FormLabel>Rooms</FormLabel>
      </div>
      <div style={{ maxWidth: "600px" }}>
        <Toggle
          checked={pets}
          label="Pets"
          onChange={(e) => setPets(e.target.checked)}
        />
        {pets && <TextField label="What kinda pets" onChange={(e) => setPetInfo(e.target.value)} placeholder="" />}
      </div>
      <Toggle
        checked={vacuum}
        label="Vacuum"
        onChange={(e : any) => setVacuum(e.target.checked)}
      />
      <Toggle
        checked={supplies}
        label="Cleaning supplies (+3e if not)"
        onChange={(e) => setSupplies(e.target.checked)}
      />
      <TextField label="Allergies" onChange={(e) => setAllergies(e.target.value)} />
      <TextField label="Other: directions etc." onChange={(e) => setOther(e.target.value)} />
    </form>
  )
}

export default ServiceForm
