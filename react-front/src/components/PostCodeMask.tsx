/* Joonatan Kuosa
 * 2020-01-11
 *
 * Akka - cleaning service app
 */
import React from 'react'

import MaskedInput from 'react-text-mask'

interface PostCodeMaskProps {
  inputRef: (ref: HTMLInputElement | null) => void
}

function PostCodeMask(props: PostCodeMaskProps) {
  const { inputRef, ...other } = props

  return (
    <MaskedInput
      {...other}
      ref={(ref: any) => {
        inputRef(ref ? ref.inputElement : null)
      }}
      mask={[/\d/, /\d/, /\d/, /\d/, /\d/]}
    />
  )
  //placeholderChar={'\u2000'}
}

export default PostCodeMask
