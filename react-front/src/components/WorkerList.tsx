/* Joonatan Kuosa
 * 2020-01-02
 *
 * Akka - cleaning service app
 */
import React, { useState, useEffect } from 'react'

import { Grid, Button } from '@material-ui/core'

import { IWorker } from '../types'
import Contractor from './Contractor'

const WORKERS_PER_PAGE = 3

const WorkerList = ( props: {
  workers : IWorker[],
  previewWorkerCb : (worker: IWorker) => void,
  workerSelectedCb : (worker : IWorker | undefined) => void,
}) => {
  const [workerPage, setWorkerPage] = useState<number>(0)
  const [selectedWorker, setSelectedWorker] = useState<IWorker | undefined>(undefined)

  const { workers, previewWorkerCb, workerSelectedCb} = props

  // propagate it upwards for the order
  useEffect(() => {
    workerSelectedCb(selectedWorker)
  }, [selectedWorker, workerSelectedCb])

  // Need to reset the page when parent changes worker list
  useEffect(() => {
    if(workers.length / WORKERS_PER_PAGE <= workerPage) {
      setWorkerPage(0)
    }
  }, [workers])

  const next_page_avail = ((workerPage+1) * WORKERS_PER_PAGE < workers.length)
  const prev_page_avail = (workerPage > 0)

  const nextWorkerPage = () : void => {
    if (next_page_avail) {
      setWorkerPage(workerPage + 1)
    }
  }

  const prevWorkerPage = () : void => {
    if (prev_page_avail) {
      setWorkerPage(workerPage - 1)
    }
  }

  return (
    <Grid container direction="column" spacing={1}>
      <p>{workers.length} workers available. Select the worker you fancy</p>
      {prev_page_avail && <Button onClick={prevWorkerPage}>Previous</Button>}
      {workers.slice(workerPage*WORKERS_PER_PAGE, (workerPage+1)*WORKERS_PER_PAGE)
        .map((x : IWorker) => (
          <Grid item key={x.id} >
            <Contractor
              worker={x}
              previewWorker={previewWorkerCb}
              workerSelectedCb={setSelectedWorker}
              selected={(selectedWorker ? selectedWorker.name : '') === x.name}
            />
          </Grid>
        ))
      }
      {next_page_avail && <Button onClick={nextWorkerPage}>More</Button>}
    </Grid>
  )
}

export default WorkerList
