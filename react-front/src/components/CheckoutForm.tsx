/* Joonatan Kuosa
 * 2020-01-08
 *
 * Akka - cleaning service app
 */
import React, { useState, useEffect } from 'react'
import { Theme, makeStyles } from '@material-ui/core/styles'
import { Grid, Button, FormLabel } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

import {
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
  Elements,
  injectStripe,
} from 'react-stripe-elements'

import axios from 'axios'
import qs from 'qs'

// Testing hard-coded key
// TODO replace this with a axios call to the server
//  or move this whole thing to the backend and behind an API call
const STRIPE_SECRET_KEY = 'sk_test_8t0L0c7RGno4I2C5gaBqZ1mb00OYVK34L0'

const useStyles = makeStyles((theme : Theme) => ({
  // base and invalid are react-stripe classes
  base: {
    // Quick and dirty solution for proper sized credit card box
    width: '20rem',
    minWidth: '20rem',
    fontSize: '24px',
    color: '#424770',
    letterSpacing: '0.025em',
    fontFamily: 'Source Code Pro, monospace',
    '::placeholder': {
      color: '#aab7c4',
    },
    paddingTop: 10,
    paddingBottom: 10,
  },
  invalid: {
    color: '#9e2146',
  },
  root: {
    margin: theme.spacing(1),
    width: '90vw',
    maxWidth: '600px',
  },
}))

const handleBlur = () => {
  console.log('[blur]')
}
const handleChange = (change :any) => {
  console.log('[change]', change)
}
const handleFocus = () => {
  console.log('[focus]');
}
const handleReady = () => {
  console.log('[ready]');
}

// TODO alerts using Flash
const confirmPayment = (params: {cb : any, stripe : any, elements: any, address : any, client_secret : string}) => {
  console.log('confirmPayment')
  const { cb, stripe, elements, client_secret } = params

  const card = elements.getElement('cardNumber')

  const { address } = params
  if (stripe) {
    // billing_details.address { city, country, line1, line2, postal_code }
    // billing_details.email
    // billing_details.name
    stripe.confirmCardPayment(client_secret, {
      payment_method: {
        card: card,
        billing_details: {
          name: address.name,
          address: {
            postal_code: address.postcode,
            line1: address.address,
            country: 'FI',
          },
        },
      }
    }).then((resp : any) => {
      // TODO type is { paymentIntent, error }, find the Stripe type and code it
      // TODO should register a callback to the Stripe API not use this
      //  this will fail if the browser window is closed etc.
      console.log('stripe response: ', resp)
      if (resp.error) {
        const msg = resp.error.code
        cb(false, msg)
      } else {
        cb(resp.paymentIntent.status === 'succeeded')
      }
    })
  } else {
    console.log("Stripe.js hasn't loaded yet.")
  }
}

// props: stripe and elements
// TODO fix the any type
const _CardForm = (props : any) => {
  const classes = useStyles()
  const [paymentIntent, setPaymentIntent] = useState<any | undefined>(undefined)
  const [error, setError] = useState<string>('')

  const { stripe, elements, address, amount } = props

  // Works
  // TODO add a prop for the amount and tie this effect to that prop
  useEffect(() => {
    axios({
      method: 'post',
      url: 'https://api.stripe.com/v1/payment_intents',
      headers: {
        'Authorization': "bearer " + STRIPE_SECRET_KEY,
        'content-type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify({
        amount: amount,
        currency: 'eur',
      }),
    }).then((response :any) => {
      console.log(response)
      const intent = response.data
      setPaymentIntent(intent)
    }).catch((error) => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }
      console.log(error.config);
      setError('failed to get intent: ' + error)
    })
  }, [amount])

  // TODO switch to material alerts
  // TODO propagate upwards,
  //    allows us to use alerts in the App
  //    and we can clear the payment details then
  const onPayment = (success : boolean, error? : string) => {
    if (success) {
    } else {
      setError('Payment failed : ' + error)
    }
    props.onPayment(success)
  }

  const handleSubmit = (evt : any) => {
    evt.preventDefault()
    console.log('handleSubmit')
    const params = {
      cb: onPayment,
      stripe: stripe,
      elements: elements,
      address: address,
      client_secret: paymentIntent.client_secret,
    }
    confirmPayment(params)
  }

  // Using a split form because the combined isn't responsive
  // TODO add card logos based on what stripe detects
  // TODO expiry and cvc shouldn't use base css class (too long text input)
  // TODO combine the three elements on desktop to a single line
  return (
    <form onSubmit={handleSubmit} className={classes.root}>
      {error !== '' && <Alert severity="error">{error}</Alert>}
      <p>Amount: {amount / 100.} €</p>
      <FormLabel>
        Card details
        <CardNumberElement
          onBlur={handleBlur}
          onChange={handleChange}
          onFocus={handleFocus}
          onReady={handleReady}
          className={classes.base}
          style={{ base: { fontSize: '18px' } }}
        />
      </FormLabel>
      <FormLabel>
        Expiration date
        <CardExpiryElement
          onBlur={handleBlur}
          onChange={handleChange}
          onFocus={handleFocus}
          onReady={handleReady}
          className={classes.base}
          style={{ base: { fontSize: '18px' } }}
        />
      </FormLabel>
      <FormLabel>
        CVC
        <CardCvcElement
          onBlur={handleBlur}
          onChange={handleChange}
          onFocus={handleFocus}
          onReady={handleReady}
          className={classes.base}
          style={{ base: { fontSize: '18px' } }}
        />
      </FormLabel>
      <Grid item style={{ textAlign: "center" }} xs={12}>
        <Button variant="contained" onClick={props.backCb}>Back</Button>
        <Button variant="contained" type="submit" color="primary">Pay</Button>
      </Grid>
    </form>
  )
}
const CardForm = injectStripe(_CardForm)

const CheckoutForm = (props : any) => {
  // TODO add other payment methdods (SEPA)
  return (
    <div>
      <Elements>
        <CardForm onPayment={props.onPayment} address={props.address} amount={props.amount} backCb={props.backCb} />
      </Elements>
    </div>
  )
}

export default injectStripe(CheckoutForm)
