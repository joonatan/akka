/* Joonatan Kuosa
 * 2020-01-08
 *
 * Akka - cleaning service app
 */
import React, { useEffect } from 'react'

import { Grid, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

import { StripeProvider, Elements } from 'react-stripe-elements'

import CheckoutForm from './CheckoutForm'
import AddressForm, { IAddress, isAddressValid } from './AddressForm'
import ServiceForm, { IService } from './ServiceForm'

const STRIPE_PUBKEY = "pk_test_Ixs8gVK7BZR5Y7pu2DW1ZKwh004ATiS0Tb"

enum Tab {
  Services = 0,
  Address,
  Payment,
}

// TODO prefill postcode from before
const OrderPage = (props : { postcode: string, backCb : () => void, nextCb: () => void } ) => {
  const [service, setService] = React.useState<IService | null>(null)
  const [address, setAddress] = React.useState<IAddress | null>(null)
  const [stripe, setStripe] = React.useState<any | null>(null)

  const [expanded, setExpanded] = React.useState<Tab>(Tab.Services)

  useEffect(() => {
    if (window.Stripe) {
      setStripe(window.Stripe(STRIPE_PUBKEY))
    } else {
      const s = document.querySelector('#stripe-js')
      if (s) {
        s.addEventListener('load', () => {
          // Create Stripe instance once Stripe.js loads
          setStripe(window.Stripe(STRIPE_PUBKEY))
        })
      }
    }
  }, [])

  const handleService = (rs : IService) => {
    console.log(rs)
    setService(rs)
  }

  const handleAddress = (rs : IAddress) => {
    console.log(rs)
    setAddress(rs)
  }

  // TODO calculate the payment amount from IService
  const amount = 9099

  const handlePayment = (success : boolean) => {
    if (success) {
      console.log('OrderPage : handlePayment succesfully')
      props.nextCb()
    } else {
      console.log('OrderPage : handlePayment failed')
    }
  }

  const handleChange = (panel: Tab) => (event: React.ChangeEvent<{}>, newExpanded: boolean) => {
    setExpanded(panel)
    // TODO set focus to the first line of the component
    // TODO check valid state transition (Address needs to be valid to open Payment)
  }

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: '80vh' }}
      >
      <ExpansionPanel square expanded={expanded === Tab.Services} onChange={handleChange(Tab.Services)}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          Services
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <ServiceForm cb={handleService} />
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel square expanded={expanded === Tab.Address} onChange={handleChange(Tab.Address)}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          Address
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <AddressForm postcode={props.postcode} cb={handleAddress} />
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel square expanded={expanded === Tab.Payment} onChange={handleChange(Tab.Payment)}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          Payment
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <StripeProvider stripe={stripe}>
            <Elements>
              <CheckoutForm address={address} onPayment={handlePayment} amount={amount} backCb={props.backCb} />
            </Elements>
          </StripeProvider>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </Grid>
  )
}

export default OrderPage
