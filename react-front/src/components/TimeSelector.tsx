/* Joonatan Kuosa
 * 2020-01-07
 *
 * Akka - cleaning service app
 */
import React, { useState, useEffect } from 'react'

import { makeStyles, Theme, FormControlLabel, FormControl, FormGroup, FormLabel, FormHelperText, Checkbox } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
  timeSelection: {
    margin: 'auto',
    paddingTop: 10,
  }
}))

const TimeSelector = (props : { cb : (time : any) => void }) => {
  const [time, setTime] = useState({
    morning: true,
    afternoon: true,
    evening: true,
  })

  useEffect(() => {
    props.cb(time)
  }, [time, props])

  const classes = useStyles()

  const handleTimeChange = (name: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setTime({ ...time, [name]: event.target.checked })
  }

  return (
    <FormControl fullWidth={true} component="fieldset">
      <FormLabel component="legend"  className={classes.timeSelection}>Good time for you?</FormLabel>
      <FormGroup className={classes.timeSelection}>
        <FormControlLabel
          control={<Checkbox checked={time.morning} onChange={handleTimeChange('morning')} value="morning" />}
          label="Morning"
        />
        <FormControlLabel
          control={<Checkbox checked={time.afternoon} onChange={handleTimeChange('afternoon')} value="afternoon" />}
          label="Afternoon"
        />
        <FormControlLabel
          control={<Checkbox checked={time.evening} onChange={handleTimeChange('evening')} value="evening" />}
          label="Evening"
        />
      </FormGroup>
      <FormHelperText></FormHelperText>
    </FormControl>
  )
}

export default TimeSelector
