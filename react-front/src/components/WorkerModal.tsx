/*  Joonatan Kuosa
 *  2020-01-02
 *
 * Akka - cleaning service app
 */
import React from 'react'

import { Grid, Modal, IconButton, makeStyles, Typography } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import Rating from '@material-ui/lab/Rating'

import WeeklySchedule from './WeeklySchedule'

import { IWorker } from '../types'

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    backgroundColor: '#dddddd',
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    // middle of the screen
    maxWidth: '1000px',
    maxHeight: '90vh',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
  image : {
    maxHeight: 400,
  },
  title: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  rating: {
    marginLeft: 'auto',
  },
}))

const WorkerModal = (props : { open : boolean, closeModal : () => void, worker? : IWorker | undefined }) => {
  const classes = useStyles()

  const { open, closeModal, worker } = props

  if (!worker) {
    return null
  }

  // TODO add a select button
  // TODO add side buttons to move to the next worker (and arrow key support)
  return (
    <Modal
      aria-labelledby="worker-modal-title"
      aria-describedby="worker-modal-description"
      open={open}
      onClose={() => closeModal()}
    >
      <Grid container className={classes.paper} justify="flex-start" spacing={3}>
        <Grid item xs={12} sm={4}>
          <img src={worker.picture} alt='' className={classes.image} />
        </Grid>
        <Grid item xs={12} sm={8}>
          <div className={classes.title}>
            <Typography gutterBottom variant="h5" component="h2">
              {worker.name}
            </Typography>
            <Rating className={classes.rating} name="read-only" value={worker.rating} precision={0.5} readOnly />
          </div>
          <Typography gutterBottom variant="body2" component="p" id="worker-modal-description">
            {worker.desc}
          </Typography>
          <Typography gutterBottom variant="body2" component="p" id="worker-modal-description">
            {worker.info}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <WeeklySchedule calendar={worker.calendar} />
        </Grid>
        <Grid item xs={12}>
        <IconButton
          key="close"
          aria-label="close"
          color="inherit"
          onClick={() => closeModal()}
          style={{ float: 'right' }}
        >
          <CloseIcon />
        </IconButton>
        </Grid>
      </Grid>
    </Modal>
  )
}

export default WorkerModal
