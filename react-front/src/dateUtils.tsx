const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

const today = new Date()
const monday : Date = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 1)
const sunday : Date = new Date(monday.getFullYear(), monday.getMonth(), monday.getDate() + 6)

const getDate = (start : Date, offset : number) => {
  return new Date(start.getFullYear(), start.getMonth(), start.getDate() + offset)
}

const eqDate = (d1 : Date, d2 : Date) => {
  return d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate()
}

const max = (d1 : Date, d2 : Date) => {
  return d1 >= d2 ? d1 : d2
}

const isSameMonth = (d1 : Date, d2 : Date) => {
  return d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth()
}

export {
  days,
  today,
  monday,
  sunday,
  getDate,
  eqDate,
  max,
  isSameMonth,
}
